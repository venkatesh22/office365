# Dealing with tokens
import pprint
import random
from office365.models import Token
token = Token.objects.all().order_by('-id')[0]
from office365.handlers import Office365Handler
token = Office365Handler.refresh_token(token)

# Discover service
from office365 import discovery
endpoint = discovery.build('Contacts', credentials=token.oauth_token)

# Play with contacts
print '---------------------'
print 'Contacts'
contacts = endpoint.Contact.contacts()
if len(contacts) == 0:
    endpoint.Contact.add_contact({
       "GivenName" : "John",
       "EmailAddresses" : [ 
           { "Address": "John@contoso.com", "Name" : "John" }
       ],
       "BusinessPhones" : [
           "123-456-7890"
       ]
    })
    contacts = endpoint.Contact.contacts()
else:
    contact = contacts[0]
    contact.MobilePhone1 = random.randint(0, 100000000)
    endpoint.Contact.update_contact(contact)

pprint.pprint(endpoint.Contact.contacts()[0].to_data())

# Play with events
endpoing = discovery.build('Calendar', credentials=token.oauth_token)


# Get mail
print '---------------------'
print 'Folders'
endpoint = discovery.build('Mail', credentials=token.oauth_token)
folders = endpoint.Folder.folders()
for folder in folders:
    pprint.pprint(folder)

print '---------------------'
print 'Messages'
messages = endpoint.Folder.messages()
for message in message:
    pprint.pprint(message)

# Get events data
print '---------------------'
print 'Folders'
endpoint = discovery.build('Calendar', credentials=token.oauth_token)
events = endpoint.Event.events()
for event in events:
    pprint.pprint(event)
