"""Model objects for requests and responses.
Each API may support one or more serializations, such
as JSON, Atom, etc. The model classes are responsible
for converting between the wire format and the Python
object representation.
"""
import json

dump_request_response = False


class ValidationError(Exception):
    pass


class Field(object):
    """Represent a field
    """

    def __init__(self, help_text, writable=True):
        self.help_text = help_text
        self.writable = writable

    def update_with_name(self, name):
        """Update field definition with a name
        """
        self.name = name

    def validate(self):
        """Should be implemented in child classes
        """
        raise NotImplemented("validate should be implemented")


class String(Field):
    """Class represents a string
    """

    def validate(self, value):
        """Should be a string
        """
        if not isinstance(value, (str, unicode)):
            raise ValidationError("")
        return True

class int32(Field):
    """Represents integer value
    """

    def validate(self, value):
        try:
            int(value)
        except:
            raise ValidationError("")


class EmailAddressField(String):
    """
    """


class DateTimeOffset(Field):
    """Class represents datetimeoffset
    """

    def validate(self, value):
        """
        """


class Collection(Field):
    """Represent typed collection
    """

    def __init__(self, collection_type, help_text, writable=True):
        if not collection_type == 'self':
            self.collection_type = collection_type(help_text, writable)
        self.help_text = help_text
        self.writable = writable

    def validate(self, value):
        return map(self.collection_type.validate, value)


class ModelMeta(type):
    """Meta class
    """

    def __new__(cls, name, bases, attrs):
        super_new = super(ModelMeta, cls).__new__
        # Create the class.
        module = attrs.pop('__module__')
        new_class = super_new(cls, name, bases, {'__module__': module})

        # Move fields to meta object
        fields = {}
        for field in attrs.keys():
            if isinstance(attrs[field], Field):
                fields[field] = attrs.pop(field)
                # recursive collections
                if (isinstance(fields[field], Collection) and
                      not hasattr(fields[field], 'collection_type')):
                    fields[field].collection_type = new_class

        # Add to class
        attrs['_meta'] = type('Meta', (object,), {'fields': fields})
        for class_field in attrs:
            setattr(new_class, class_field, attrs[class_field])
        return new_class


class Model(Field):
    """Represents a model
    """
    __metaclass__ = ModelMeta

    def validate(self):
        """Validate all the fields before sending data
        """
        for field_name in self._meta.fields:
            value = getattr(self, field_name, None)
            self._meta.fields[field_name].validate(value)
        return True

    @classmethod
    def from_data(cls, *args, **kwargs):
        """Get item from data
        """
        # Check arguemnts and retrieve value to unpack
        if len(args) > 1:
            raise Exception('To many arguments')
        elif len(args) == 1:
            data = args[0]
            if data is None:
                return None
            if not isinstance(data, dict):
                if isinstance(data, Model):
                    return data
                else:
                    raise Exception('Unsupported argument')
        else:
            data = kwargs
        # Create instance and unpack data
        instance = cls(cls.__doc__)
        for field_name in cls._meta.fields:
            if field_name in data:
                value = data[field_name]
                field = cls._meta.fields[field_name]
                if isinstance(field, Collection):
                    nested_model = field.collection_type
                    if isinstance(nested_model, Model):
                        value = [nested_model.from_data(item)
                                 for item in value]
                elif isinstance(field, Model):
                    value = field.from_data(value)
                setattr(instance, field_name, value)
        return instance

    def to_data(self):
        """Convert object into dictionary
        """
        result = {}
        for field_name in self._meta.fields:
            if hasattr(self, field_name):
                value = getattr(self, field_name)
                field = self._meta.fields[field_name]
                if isinstance(field, Collection):
                    if isinstance(field.collection_type, Model):
                        result[field_name] = [item.to_data() if item else None
                                              for item in value]
                    else:
                        result[field_name] = value
                elif isinstance(value, Model):
                    result[field_name] = value.to_data()
                else:
                    result[field_name] = value
        return result

    def serialize(self):
        """Get a string representation
        """
        self.validate()
        return json.dumps(self.to_data())


class EmailAddress(Model):
    """The name and email address of a contact or message recipient.

    Type = Microsoft.OutlookServices.EmailAddress
    """
    Name = String("The display name of the person or entity")
    Address = String("The email address of the person or entity")


class PhysicalAddress(Model):
    """The physical address of a contact.
    """
    Street = String("The street")
    City = String("The city")
    State = String("The state")
    CountryOrRegion = String("The country or region")
    PostalCode = String("The postal code")


class Contact(Model):
    '''Represents a single contact
    '''
    AssistantName = String("The name of the contact's assistant")
    Birthday = DateTimeOffset("The contact's birthday")
    BusinessAddress = PhysicalAddress("The contact's business address")
    BusinessHomePage = String("The business home page of the contact")
    BusinessPhones = Collection(String, "The contact's business phone numbers")
    Categories = Collection(String,
                            "The categories associated with the contact")
    ChangeKey = String("Identifies the version of the contact. Every time the contact is changed, ChangeKey changes as well. This allows Exchange to apply changes to the correct version of the object")
    CompanyName = String("The name of the contact's company")
    Department = String("The contact's department")
    DateTimeCreated = DateTimeOffset("The time the contact was created", False)
    DateTimeLastModified = DateTimeOffset("The time the contact was modified",
                                          False)
    DisplayName = String("The contact's display name")
    EmailAddresses = Collection(EmailAddress, "The contact's email addresses")
    FileAs = String("The name the contact is filed under")
    Generation = String("The contact's generation")
    GivenName = String("The contact's given name")
    HomeAddress = PhysicalAddress("The contact's home address")
    HomePhones = Collection(String, "The contact's home phone numbers")
    Id = String("The contact's unique identifier", False)
    ImAddresses = Collection(String,
                             "The contact's instant messaging (IM) addresses")
    Initials = String("The contact's initials")
    JobTitle = String("The contact's job titles")
    Manager = String("The name of the contact's manager")
    MiddleName = String("The contact's middle name")
    MobilePhone1 = String("The contact's mobile phone number")
    NickName = String("The contact's nickname")
    OfficeLocation = String("The location of the contact's office")
    OtherAddress = PhysicalAddress("Other addresses for the contact")
    ParentFolderId = String("The ID of the contact's parent folder", False)
    Profession = String("The contact's profession")
    Surname = String("The contact's surname")
    Title = String("The contact's title")
    YomiCompanyName = String("The phonetic Japanese company name of the contact. This property is optional")
    YomiGivenName = String("The phonetic Japanese given name (first name) of the contact. This property is optional")
    YomiSurname = String("The phonetic Japanese surname (last name) of the contact. This property is optional.")


class ContactFolder(Model):
    """A folder that contains contacts.

    Type: Microsoft.OutlookServices.ContactFolder

    A ContactFolder collection returns an array of contact folders in the value
    property of the OData response. Use $count to get the count of entities in
    the collection: .../me/contactfolders/$count
    """
    ChildFolders = Collection("self",
          "The collection of child folders in the folder. Navigation property",
          False)
    Contacts = Collection(Contact,
                          "The contacts in the folder. Navigation property",
                          False)
    DisplayName = String("The folder's display name")
    Id = String("Unique identifier of the contact folder", False)
    ParentFolderId = String("The ID of the folder's parent folder.")


class ResponseStatus(Model):
    """The response status of a meeting request.

    Type: Microsoft.OutlookServices.ResponseStatus
    """
    Response = String("The response type: None = 0, Organizer = 1, TentativelyAccepted = 2, Accepted = 3, Declined = 4, NotResponded = 5.")
    Time = DateTimeOffset("Time")


class Attendee(Model):
    """An event attendee.

    Type: Microsoft.OutlookServices.Attendee
    """
    EmailAddress = EmailAddress('The name and email address of the attendee.')
    Status = ResponseStatus(
                    "The response (none, accepted, declined, etc.) and time.")
    Type = String("The attendee type: Required=0, Optional=1, Resource=2")


class Recipient(Model):
    """A sender or receiver for a message.

    Type: Microsoft.OutlookServices.Recipient
    """
    EmailAddress = EmailAddress("The recipient's email address.")


class RecurrencePattern(Model):
    """The frequency of an event.

    Type: Microsoft.OutlookServices.RecurrencePattern
    """
    Type = String("The recurrence pattern type: Daily = 0, Weekly = 1, AbsoluteMonthly = 2, RelativeMonthly = 3, AbsoluteYearly = 4, RelativeYearly = 5.")
    Interval = int32(
        "The number of units of a given recurrence type between occurrences.")
    DayOfMonth = int32("The day of month that the item occurs on.")
    Month = int32(
        "The month that the item occurs on. This is a number from 1 to 12.")
    DaysOfWeek = Collection(String, "A collection of days of the week: Sunday = 0, Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6.")
    FirstDayOfWeek = String("The day of the week: Sunday = 0, Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6.")
    Index = String("The week index: First = 0, Second = 1, Third = 2, Fourth = 3, Last = 4.")


class RecurrenceRange(Model):
    """The duration of an event.

    Type: Microsoft.OutlookServices.RecurrenceRange
    """
    Type = String(
                "The recurrence range: EndDate = 0, NoEnd = 1, Numbered = 2.")
    StartDate = DateTimeOffset("The start date of the series.")
    EndDate = DateTimeOffset("The end date of the series.")
    NumberOfOccurrences = int32("How many times to repeat the event.")


class PatternedRecurrence(Model):
    """The recurrence pattern and range.

    Type: Microsoft.OutlookServices.PatternedRecurrence
    """
    Pattern = RecurrencePattern("The frequency of an event.")
    Range = RecurrenceRange("The duration of an event.")


class ItemBody(Model):
    """The body content of a message or event.

    Type: Microsoft.OutlookServices.ItemBody
    """
    ContentType = String("The content type: Text = 0, HTML = 1.")
    Content = String("The text or HTML content.")


class Location(Model):
    """The location of an event.

    Type: Microsoft.OutlookServices.Location
    """
    DisplayName = String("The name associated with the location.")


class Message(Model):
    """A message in a mailbox folder.

    Type: Microsoft.OutlookServices.Message

    A Message collection returns an array of messages in the value property of
    the OData response. Use $count to get the count of entities in the
    collection: .../me/messages/$count

    See Message operations for supported actions.
    """
#    Attachments = Collection(Attachment,
#                    "The FileAttachment and ItemAttachment attachments for the message. Navigaton property", False)
    BccRecipients = Collection(Recipient, "The Bcc recipients for the message")
    Body = ItemBody("The body of the message", False)
    BodyPreview = String(
                        "The first 255 characters of the message body content",
                        False)
    Categories = Collection(String,
                            "The categories associated with the message")
    CcRecipients = Collection(Recipient, "The Cc recipients for the message")
    ChangeKey = String("The version of the message", False)
    ConversationId = String("The ID of the conversation the email belongs to", False)
    DateTimeCreated = DateTimeOffset(
                                "The date and time the message was created",
                                False)
    DateTimeLastModified = DateTimeOffset(
                            "The date and time the message was last changed",
                            False)
    DateTimeReceived = DateTimeOffset(
                                "The date and time the message was received",
                                False)
    DateTimeSent = DateTimeOffset("The date and time the message was sent",
                                  False)
    From = Recipient("The entity that sent the message")
#    HasAttachments  boolean Indicates whether the message has attachments", False)
#    Importance  Importance  The importance of the message: Low = 0, Normal = 1, High = 2"
#    IsDeliveryReceiptRequested  boolean Indicates whether a read receipt is requested for the message", False)
#    IsDraft boolean Indicates whether the message is a draft. A message is a draft if it hasn't been sent yet", False)
#    IsRead  boolean Indicates whether the message has been read"
#    IsReadReceiptRequested  boolean Indicates whether a read receipt is requested for the message", False)
    ParentFolderId = String(
                    "The unique identifier for the message's parent folder",
                    False)
    ReplyTo = Collection(Recipient, "The email addresses to use when replying")
    Sender = Recipient(" The sender of the message")
    Subject = String("The subject of the message", False)
    ToRecipients = Collection(Recipient, "The To recipients for the message")
#    UniqueBody  ItemBody  The body of the message that is unique to the conversation", False)

class SendMail(Model):
    Message = Message("The collection of messages in the folder")

class Folder(Model):
    """folder in a user's mailbox, such as Inbox, Drafts, and Sent Items.
    Folders can contain messages and other folders.

    Type: Microsoft.OutlookServices.Folder

    An Folder collection returns an array of folders in the value property of
    the OData response. Use $count to get the count of entities in the
    collection: .../me/folders/$count

    See Folder operations for supported actions.
    """
    ChildFolderCount = int32("The number of folders in the folder", False)
    ChildFolders = Collection('self',
                              "The collection of child folders in the folder",
                              False)
    DisplayName = String("The folder's display name")
    Id = String("The folder's unique identifier. You can use the following well-known names to access the corresponding folder: Inbox, Drafts, SentItems, DeletedItems",
                False)
    Messages = Collection(Message, "The collection of messages in the folder",
                          False)
    ParentFolderId = String(
                        "The unique identifier for the folder's parent folder",
                        False)


class Event(Model):
    """An event in a calendar.

    Type: Microsoft.OutlookServices.Event

    An Event collection returns an array of events in the value property of the
    OData response. Use $count to get the count of entities in the collection:
    .../me/events/$count

    See Event operations for supported actions.
    """
#    Attachments = Collection(Attachment,
#                             "The collection of FileAttachment and ItemAttachment attachments for the event. Navigation property",
#                             False)
    Attendees = Collection(Attendee,
                           "The collection of attendees for the event")
    Body = ItemBody("The body of the message associated with the event")
    BodyPreview = String("The preview of the message associated with the event", False)
#    Calendar = Calendar(
#            "The calendar that contains the event. Navigation property", False)
    Categories = Collection(String, "The categories associated with the event")
    ChangeKey = String("Identifies the version of the event object. Every time the event is changed, ChangeKey changes as well. This allows Exchange to apply changes to the correct version of the object", False)
    DateTimeCreated = DateTimeOffset(
                                "The date and time that the event was created",
                                False)
    DateTimeLastModified = DateTimeOffset(
                        "The date and time that the event was last modified",
                        False)
    End = DateTimeOffset("The date and time that the event ends")
#    HasAttachments  boolean Set to true if the event has attachments", False)
#    Importance  Importance  The importance of the event: Low = 0, Normal = 1, High = 2")
    Instances = Collection('self',
                           "The instances of the event. Navigation property",
                           False)
    IsAllDay = String('boolean Set to true if the event lasts all day')
#    IsCancelled boolean Set to true if the event has been canceled")
#    IsOrganizer boolean Set to true if the message sender is also the organizer")
    Location = Location("The location of the event")
    Organizer = Recipient("The organizer of the event")
    Recurrence = PatternedRecurrence("The recurrence patern for the event")
#    ResponseRequested   boolean Set to true if the sender would like a response when the event is accepted or declined")
    SeriesMasterId = String("The categories assigned to the item")
    ShowAs = String("The status to show: Free = 0, Tentative = 1, Busy = 2, Oof = 3, WorkingElsewhere = 4, Unknown = -1")
    Start = DateTimeOffset("The start time of the event")
    Subject = String("The text of the event's subject line")
    Type = String("The event type: SingleInstance = 0, Occurrence = 1, Exception = 2, SeriesMaster = 3.")
    Id = String("The contact's unique identifier", False)

class Calendar(Model):
    """A calendar. Calendars are contained in calendar groups.

    Type: Microsoft.OutlookServices.Calendar

    A Calendar collection returns an array of calendars in the value property
    of the OData response. Use $count to get the count of entities in the
    collection: .../me/calendars/$count

    See Calendar operations for supported actions.
    """
    Name = String("The calendar name")
    CalendarView = Collection(Event,
                    "The calendar view for the calendar. Navigation property",
                    False)
    ChangeKey = String("Identifies the version of the calendar object. Every time the calendar is changed, ChangeKey changes as well. This allows Exchange to apply changes to the correct version of the object",
                       False)
    Id = String("The group's unique identifier", False)
    Events = Collection(Event,
                        "The events in the calendar. Navigation property",
                        False)


class CalendarGroup(Model):
    """A group of calendars.

    Type: Microsoft.OutlookServices.CalendarGroup

    A CalendarGroup collection returns an array of calendar groups in the value
    property of the OData response. Use $count to get the count of entities in
    the collection: .../me/calendargroups/$count

    See CalendarGroup operations for supported actions.
    """
    Name = String("The group name")
    ChangeKey = String("Identifies the version of the calendar group. Every time the calendar group is changed, ChangeKey changes as well. This allows Exchange to apply changes to the correct version of the object",
                       False)
    ClassId = String("The class identifier", False)
    Id = String("The group's unique identifier", False)
    Calendars = Collection(Calendar,
                    "The calendars in the calendar group. Navigation property",
                    False)
