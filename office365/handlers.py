'''MS Live related handler classes
'''
import json
import urllib
import urlparse

from django.core import exceptions, urlresolvers
from django.shortcuts import redirect

from office365 import conf, utils


class BaseHandler(object):
    '''Base class for handling social auth requests
    '''
    profile_url = None
    service_name = None
    consumer_key = None
    consumer_secret = None
    requires_profile = True

    @classmethod
    def make_request(cls, url, token, **kwargs):
        '''Override this method to make a request to a service
        '''
        raise NotImplemented('make_request is not implemented for %s' %
                             cls.__name__)

    @classmethod
    def get_auth_url(cls, token):
        '''Override this method for getting url for authentication
        '''
        raise NotImplemented('get_auth_url is not implemented for %s' %
                             cls.__name__)

    @classmethod
    def redirect_view(cls):
        '''View for redirect
        '''
        @utils.catch_exception
        def view(request):
            '''A view function that gets a request token from a service and
            redirects user to a service authorization page
            '''
            print cls.get_auth_url(request)
            return redirect(cls.get_auth_url(request))
        return view

    @classmethod
    def callback_view(cls):
        '''Returns a view called on callback
        '''
        raise NotImplemented('callback_view is not implemented for %s' %
                             cls.__name__)


class OAuth2Handler(BaseHandler):
    '''Change the usage flow for OAuth2
    '''
    access_token_url = None
    scopes = None
    access_token_method = 'GET'
    access_token_name = 'access_token'
    callback_request_hook = None

    @classmethod
    def get_params(cls, request, extra_params):
        params = {
            'client_id': cls.consumer_key
        }
        if cls.scopes:
            params['scope'] = ','.join(cls.scopes)
        params.update(extra_params)
        return urllib.urlencode(params)

    @classmethod
    def callback_view(cls):
        '''Returns a view called on callback
        '''
        @utils.catch_exception
        def view(request):
            '''Get user info from twitter
            '''
            # Get request token
            import ipdb;ipdb.set_trace()
            if not 'code' in request.GET:
                raise exceptions.ValidationError('No code in request')
            code = request.GET['code']
            if cls.callback_request_hook:  # Get additional data if needed
                data = cls.callback_request_hook(request)
            else:
                data = ()
            data += (
                ('code', code),
                ('client_id', cls.consumer_key),
                ('client_secret', cls.consumer_secret),
                ('grant_type', 'authorization_code'),
                ('redirect_uri', request.build_absolute_uri(
                                    urlresolvers.reverse(cls.service_name))),
            )
            access_token = cls.get_access_token(data)
            # Call a hook
            if cls.callback_hook:
                cls.callback_hook(access_token)
            return access_token
        return view

    @classmethod
    def get_access_token(cls, data, token_refresh=None, **kwrags):
        '''Get a new access token
        '''
        # Make a request to server
        qs = urllib.urlencode(data)
        if cls.access_token_method == 'POST':
            handle = urllib.urlopen(cls.access_token_url, qs)
        else:
            handle = urllib.urlopen(cls.access_token_url + '?' + qs)
            print cls.access_token_url + '?' + qs
        # Handle the response and parse the results
        data = ''.join(handle.readlines())
        info = handle.info()
        if 'application/json' in info.get('Content-Type', ''):
            response = json.loads(data)
        else:
            response = dict(urlparse.parse_qsl(data))
        if not handle.getcode() == 200:
            cls.handle_error(response, handle.getcode())
        handle.close()
        # Save an access token
        import pprint
        pprint.pprint(response)
        acc_token = {
            'oauth_token': response['access_token'],
            'oauth_expires_in': response.get('expires',
                                             response.get('expires_in', 3600)),
            'token_source': cls.service_name,
            'oauth_token_refresh': response.get('oauth_token_refresh',
                                                response.get('refresh_token',
                                                             token_refresh)),
        }
        acc_token.update(kwrags)
        access_token = utils.save_token(acc_token)
        return access_token

    @classmethod
    def handle_error(cls, error, code=None):
        raise Exception(str(error))

    @classmethod
    def process_response(cls, handle, data):
        return json.loads(data)

    @classmethod
    def make_request(cls, url, token, **kwargs):
        '''Make request to Facebook api
        '''
        qs = urllib.urlencode([(cls.access_token_name, token.oauth_token)] +
                              list(kwargs.iteritems()))
        handle = urllib.urlopen(url + '?' + qs)
        data = ''.join(handle.readlines())
        if not handle.getcode() == 200:
            if 'application/json' in handle.info().get('Content-Type', ''):
                cls.handle_error(json.loads(data))
            else:
                cls.handle_error(data)
        handle.close()
        response = cls.process_response(handle, data)
        return response

    @classmethod
    def refresh_token(cls, token, **kwargs):
        '''Get a new access token using a parameters of a current token
        '''
        data = {
            'grant_type': 'refresh_token',
            'client_id': cls.consumer_key,
            'client_secret': cls.consumer_secret,
            'refresh_token': token.oauth_token_refresh
        }
        data.update(kwargs)
        long_lived_token = cls.get_access_token(data,
                                                token.oauth_token_refresh)
        cls.callback_hook(long_lived_token)
        return long_lived_token

    @classmethod
    def callback_hook(cls, token):
        '''Called on profile was got from a service before profile processing.
        Apply refreshing access token just in time when a previous token
        expires
        '''
        from . import tasks
        tasks.refresh_token.apply_async(args=[token.pk],
                                        eta=token.oauth_expires)


class LiveHandler(OAuth2Handler):
    '''Implement ouath using a MS Live API
    '''
    consumer_key = conf.MSLIVE_CLIENT_ID
    consumer_secret = conf.MSLIVE_SECRET_KEY
    auth_base_url = 'https://login.live.com'
    access_token_url = '%s/oauth20_token.srf' % auth_base_url
    access_token_method = 'GET'
    api_base_url = 'https://apis.live.net/v5.0'
    profile_url = '%s/me' % api_base_url
    service_name = 'mslive'
    template_name = '%s.html' % service_name
    scopes = conf.MSLIVE_SCOPES

    @classmethod
    def get_auth_url(cls, request):
        response = '%s/oauth20_authorize.srf?%s' % (cls.auth_base_url,
                                cls.get_params(request, response_type='code',
                                               scope=' '.join(cls.scopes)))
        return response

    @classmethod
    def refresh_token(cls, token, **kwargs):
        '''Get a new access token using a parameters of a current token
        '''
        return super(LiveHandler, cls).refresh_token(token,
                        redirect_uri=(conf.MSLIVE_REDIRECT_URL +
                                      urlresolvers.reverse(cls.service_name)))


class Office365Handler(OAuth2Handler):
    '''Implement oauth using Azure AD authorization API (used for Office365)
    '''
    consumer_key = conf.OFFICE365_CLIENT_ID
    consumer_secret = conf.OFFICE365_SECRET_KEY
    auth_base_url = ('https://login.windows.net/%s/oauth2' %
                     conf.OFFICE365_TENANT_ID)
    access_token_method = 'POST'
    access_token_url = '%s/token' % auth_base_url
    service_name = 'azuread'
    template_name = '%s.html' % service_name
    scopes = []

    @classmethod
    def get_auth_url(cls, request, params={}):
        '''Get authentication url
        '''
        params['resource'] = conf.OFFICE365_APP_ID_URI
        params['response_type'] = 'code'
        response = '%s/authorize?%s' % (cls.auth_base_url,
                                cls.get_params(request, params))
        return response

    @classmethod
    def callback_request_hook(cls, request):
        '''Add resource into request data
        '''
        return (
            ('resource', conf.OFFICE365_APP_ID_URI,),
        )


    @classmethod
    def refresh_token(cls, token, **kwargs):
        '''Get a new access token using a parameters of a current token
        '''
        return super(Office365Handler, cls).refresh_token(token,
                        redirect_uri=(conf.OFFICE365_REDIRECT_URL +
                                      urlresolvers.reverse(cls.service_name)))
