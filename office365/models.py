from django.db import models
from django.utils.translation import ugettext_lazy as _


class Token(models.Model):
    '''Store a oauth_token/oauth_token_secret pair
    '''
    SOURCES = (
        ('live', _('Live ID')),
        ('office365', _('Office365')),
    )
    oauth_token = models.TextField()
    oauth_token_refresh = models.CharField(max_length=512, null=True)
    oauth_expires = models.DateTimeField()
    oauth_verifier = models.CharField(max_length=128, null=True)
    token_source = models.CharField(max_length=20, choices=SOURCES)
    created_date = models.DateTimeField(db_column='createdDate',
                                verbose_name=_('Created'), auto_now_add=True)
    last_modified_date = models.DateTimeField(db_column='lastModifiedDate',
                                verbose_name=_('Last Modified'), auto_now=True)

    def __str__(self):
        return ' '.join((self.oauth_token, str(self.oauth_expires)))
