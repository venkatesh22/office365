'''Client library for Office365 discovery based APIs.
'''
_all__ = [
    'build',
    'build_from_document',
    'fix_method_name',
    'key2param',
]

# Standard library imports
import json
import keyword
import logging
import os
import re
import urllib

# Third-party imports
import httplib2
import uritemplate

# Internal imports
from .errors import HttpError
from .errors import InvalidJsonError
from .errors import InvalidRequestError
from .errors import UnknownApiNameOrVersion
from . import model
from office365.utils import positional
from office365.utils import _add_query_parameter

# The client library requires a version of httplib2 that supports RETRIES.
httplib2.RETRIES = 1
import logging
logger = logging.getLogger(__name__)
URITEMPLATE = re.compile('{[^}]*}')
VARNAME = re.compile('[a-zA-Z0-9_-]+')

DISCOVERY_URI = 'https://api.office.com/discovery/{api_version}/me/allServices'
HTTP_PAYLOAD_METHODS = frozenset(['PUT', 'POST', 'PATCH'])

DEFAULT_METHOD_DOC = 'A description of how to use this function'

METHODS_MAPPING = {
    'filter': {
        'method': 'GET',
        'name': None,
        'collection': True
    },
    'get': {
        'method': 'GET',
        'args': {
            'id': model.String('object id'),
        }
    },
    'add': {
        'method': 'POST',
    },
    'update': {
        'method': 'PATCH',
        'args': {
            'id': model.String('object id'),
        }
    },
    'delete': {
        'method': 'DELETE',
        'args': {
            'id': model.String('object id'),
        }
    }
}

# Library-specific reserved words beyond Python keywords.
RESERVED_WORDS = frozenset(['body'])


import model


SCHEMAS = {
    'calendar': {
        model.Event: {
            'methods': ['filter', 'get', 'add', 'update', 'delete']
        },
        model.Calendar: {
            'methods': ['filter', 'get', 'add', 'update', 'delete']
        },
        model.CalendarGroup: {
            'methods': ['filter', 'get', 'add', 'update', 'delete']
        },
    },
    'contacts': {
        model.Contact: {
            'methods': ['filter', 'get', 'add', 'update', 'delete']
        },
        model.ContactFolder: {
#            'nested': ['contacts', 'contactforlders']
            'methods': ['filter', 'get']
        }
    },
    'mail': {
        model.Message: {
            'methods': ['filter', 'get', 'add', 'update', 'delete']
        },
        model.SendMail: {
            'methods': ['add'],
            'replace': True
        },
        model.Folder: {
            'methods': ['filter', 'get']
        }
    },
}


def fix_method_name(name):
    """Fix method names to avoid reserved word conflicts.

    Args:
        name: string, method name.

    Returns:
        The name with a '_' prefixed if the name is a reserved word.
    """
    if keyword.iskeyword(name) or name in RESERVED_WORDS:
        return name + '_'
    else:
        return name


def key2param(key):
    """Converts key names into parameter names.
    For example, converting "max-results" -> "max_results"

    Args:
        key: string, the method key name.

    Returns:
        A safe method name based on the key name.
    """
    result = []
    key = list(key)
    if not key[0].isalpha():
        result.append('x')
    for c in key:
        if c.isalnum():
            result.append(c)
        else:
            result.append('_')
    return ''.join(result)


@positional(2)
def build(serviceName, version='v1.0', http=None, credentials=None):
    """Construct a Resource for interacting with an API.
    Construct a Resource object for interacting with an API. The serviceName and
    version are the names from the Discovery service.

    Args:
        serviceName: string, name of the service.
        version: string, the version of the service.
        http: httplib2.Http, An instance of httplib2.Http or something that acts
            like it that HTTP requests will be made through.
        credentials: oauth2client.Credentials, credentials to be used for
            authentication.
    Returns:
        A Resource object with methods for interacting with the service.
    """
    params = {
        'api': serviceName,
        'apiVersion': version
    }

    if http is None:
        http = httplib2.Http(disable_ssl_certificate_validation=True)

    requested_url = uritemplate.expand(DISCOVERY_URI, params)

    # REMOTE_ADDR is defined by the CGI spec [RFC3875] as the environment
    # variable that contains the network address of the client sending the
    # request. If it exists then add that to the request for the discovery
    # document to avoid exceeding the quota on discovery requests.
    if 'REMOTE_ADDR' in os.environ:
        requested_url = _add_query_parameter(requested_url, 'userIp',
        os.environ['REMOTE_ADDR'])

    logger.info('URL being requested: GET %s' % requested_url)

    resp, content = http.request(requested_url,
                            headers={'Authorization': 'Bearer ' + credentials,
                                'Accept': 'application/json;odata=verbose'})

    if resp.status == 404:
        raise UnknownApiNameOrVersion("name: %s version: %s" % (serviceName,
                                                                version))
    if resp.status >= 400:
        raise HttpError(resp, content, uri=requested_url)

    try:
        json.loads(content)
    except ValueError, e:
        logger.error('Failed to parse as JSON: ' + str(e) + '\n' + content)
        raise InvalidJsonError(e)

    return build_from_document(serviceName, content, http=http,
                               credentials=credentials)


@positional(1)
def build_from_document(service_name, service, http=None, credentials=None):
    """Create a Resource for interacting with an API.
    Same as `build()`, but constructs the Resource object from a discovery
    document that is it given, as opposed to retrieving one over HTTP.

    Args:
        service_name: string, name of the service.
        service: string or object, the JSON discovery document describing the
            API. The value passed in may either be the JSON string or the
            deserialized JSON.
        http: httplib2.Http, An instance of httplib2.Http or something that
            acts like it that HTTP requests will be made through.
        credentials: object, credentials to be used for authentication.

    Returns:
        A Resource object with methods for interacting with the service.
    """
    if isinstance(service, basestring):
        service = json.loads(service)

    root = service['d']['results']

    # Get endpoints
    endpoints = {}
    for item in root:
        endpoints[item['Capability']] = item

    # Check api name
    if not service_name in endpoints:
        raise UnknownApiNameOrVersion("No endpoint for " + service_name)

    # Generate endpoint
    name = service_name.lower()
    #endpoint_meta = endpoints[service_name]
    #import pprint
    #pprint.pprint(endpoints)
    #pprint.pprint(endpoint_meta)
    #base_url = 'https://apis.live.net/v5.0/me'
    base_url = 'https://outlook.office365.com/api/v1.0/me/'
    #base_url = endpoint_meta['ServiceEndpointUri']
    endpoint = EndPoint(base_url, SCHEMAS[name], credentials)
    return endpoint


class Method(object):
    """Callable method wrapper
    """

    def __init__(self, resource, method, args, is_collection=False):
        self.resource = resource
        self.method = method
        self.args = args
        self.is_collection = is_collection

    def __call__(self, *args, **kwargs):
        # path-only requests
        path = None
        params = []
        arg_index = 0
        for arg in self.args:
            if arg in kwargs:
                value = kwargs[arg]
            else:
                value = params[arg_index]
                arg_index += 1
            self.args[arg].validate(value)
            params.append(value)
        path = '/'.join(params)
        if self.method in HTTP_PAYLOAD_METHODS:
            # Methods for update and add dat
            if len(args) == 1:
                item = self.resource.model_class.from_data(args[0])
                logger.info("item :: %s" % args[0])
                logger.info("==================================")
#            elif len(args) >= 2:
#                item = self.resource.model_class.from_data(args[0])
#                path = args[1]
            else:
                item = self.resource
            data = item.to_data()
            logger.info("data :: %s" % data)
            logger.info("==================================")
            response = self.resource.make_request(self.method, data=data, path=path)
            logger.info("Response :: %s" % response)
            logger.info("==================================")
        else:
            response = self.resource.make_request(self.method, path=path)
            if self.is_collection:
                return [self.resource.model_class.from_data(item)
                        for item in response]
        return self.resource.model_class.from_data(response)


class Resource(object):
    '''Represent resource with methods
    '''

    def __init__(self, endpoint, path, model_class, methods, replace=False):
        self.endpoint = endpoint
        self.model_class = model_class
        self.model = model_class(self)
        self.replace = replace
        if not self.replace:
            self.path = path + 's'
        else:
            self.path = path
        model_name = model_class.__name__.lower()
        for method_name in methods:
            mapping_data = METHODS_MAPPING[method_name]
            if 'name' in mapping_data:
                if mapping_data['name']:
                    model_method_name = mapping_data['name']
                else:
                    model_method_name = model_name + 's'
            else:
                model_method_name = method_name + '_' + model_name
            setattr(self, model_method_name,
                    Method(self, mapping_data['method'],
                           mapping_data.get('args', []),
                           'collection' in mapping_data))

    def make_request(self, method, path=None, data={}):
        print self.make_request, self.path, path
        request_path = (self.path + '/' + path) if path else self.path
        return self.endpoint.make_request(method, request_path, data)


class EndPoint(object):
    '''Declares endpoint.

    After creation it shuold contain all the methods
    '''

    def __init__(self, base_url, resources, access_token):
        self.access_token = access_token
        self.base_url = base_url
        for model_class in resources:
            resource_class_name = model_class.__name__
            print model_class, resource_class_name
            resource_name = resource_class_name.lower()
            methods = resources[model_class]['methods']
            replace = resources[model_class].get('replace', False)
            setattr(self, resource_class_name, Resource(self, resource_name,
                                                        model_class, methods, replace))

    def make_request(self, method, request_path, data=None,
                     multi_results=False):
        '''Mak request to server
        '''
        requested_url = (self.base_url + '/' + request_path
                         if request_path else self.base_url)
        http = httplib2.Http(disable_ssl_certificate_validation=True)
        # prepare credentials and request
        if not data:
            data = {}
        #data['access_token'] = self.access_token
        if not method in HTTP_PAYLOAD_METHODS:
            requested_url += '?' + urllib.urlencode(data)
            body = ''
        else:
            body = json.dumps(data)
        # Make request
        print method, requested_url, '\n'
        print body
        resp, content = http.request(requested_url, method=method, body=body,
                    headers={'Authorization': 'Bearer ' + self.access_token,
                        'Content-Type': 'application/json;odata.metadata=none',
                        'Accept-Encoding': 'gzip'})
        # Process response
        try:
            data = json.loads(content)
        except Exception as e:
            logger.error(str(e))
            raise InvalidJsonError(str(e))
        if 'error' in data:
            raise InvalidRequestError(data['error']['code'] + ': ' +
                                      data['error']['message'])
        if '@odata.context' in data and 'value' in data:
            return data['value']  # Many results returned
        return data  # Single result
