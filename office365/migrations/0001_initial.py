# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Token',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('oauth_token', models.TextField()),
                ('oauth_token_refresh', models.CharField(max_length=512, null=True)),
                ('oauth_expires', models.DateTimeField()),
                ('oauth_verifier', models.CharField(max_length=128, null=True)),
                ('token_source', models.CharField(max_length=20, choices=[(b'live', 'Live ID'), (b'office365', 'Office365')])),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created', db_column=b'createdDate')),
                ('last_modified_date', models.DateTimeField(auto_now=True, verbose_name='Last Modified', db_column=b'lastModifiedDate')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
