'''Office365 integration tasks
'''
import logging
import traceback

from celery import task

import handlers, models, utils

logger = logging.getLogger('office365.tasks')


@task.task
def refresh_token(token_id):
    '''Call token refreshing method: it does not update existing token but
    creates a new one
    '''
    token = models.Token.objects.get(pk=token_id)
    try:
        if models.Token.objects.filter(oauth_expires__gt=utils.now(),
                                    survey_session_id=token.survey_session_id,
                                    token_source=token.token_source).exists():
            raise Exception('Can\'t refresh token - there are active tokens ' +
                            'for this survey_session_id and token_source')
        handler = handlers.get_handler(token.token_source)
        return str(handler.refresh_token(token))
    except Exception as exception:
        error_message = traceback.format_exc(exception)
        logger.error(repr(token) + ':\n' + error_message)
        return str(exception)

