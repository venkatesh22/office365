from django.conf.urls import patterns, url


import handlers, views

urlpatterns = patterns('',
    # MS Live
    url(r'mslive/login.html', handlers.LiveHandler.redirect_view(),
        name='mslive_login'),
    url(r'mslive/callback.html',
        views.wrapper(handlers.LiveHandler.callback_view()),
        name='mslive'),
    # Azure
    url(r'azuread/login.html', handlers.Office365Handler.redirect_view(),
        name='azuread_login'),
    url(r'azuread/callback.html',
        views.wrapper(handlers.Office365Handler.callback_view()),
        name='azuread'),
)
