from django.shortcuts import render


def wrapper(handler_method):
  def view(request):
    token = handler_method(request)
    request.session['token'] = token.pk
    return render(request, 'signed.html')
  return view
