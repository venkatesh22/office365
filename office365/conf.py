from django.conf import settings


MSLIVE_SECRET_KEY = settings.MSLIVE_SECRET_KEY
MSLIVE_CLIENT_ID = settings.MSLIVE_CLIENT_ID
MSLIVE_REDIRECT_URL = settings.MSLIVE_REDIRECT_URL
# Scopes required by an application. Please read to get more info:
# http://msdn.microsoft.com/en-us/library/dn631845.aspx
try:
    MSLIVE_SCOPES = settings.SG_AUTH_MSLIVE_SCOPES
except AttributeError:
    MSLIVE_SCOPES = ('wl.offline_access', 'wl.signin', 'wl.basic',
                     'wl.emails', 'wl.work_profile', 'wl.birthday',
                     'wl.contacts_birthday', 'wl.phone_numbers', )

# Value of your client_Id given when you registered your application with AAD
OFFICE365_CLIENT_ID = settings.OFFICE365_CLIENT_ID
OFFICE365_SECRET_KEY = settings.OFFICE365_SECRET_KEY
OFFICE365_APP_ID_URI = settings.OFFICE365_APP_ID_URI
OFFICE365_REDIRECT_URL = settings.OFFICE365_REDIRECT_URL
OFFICE365_TENANT_ID = settings.OFFICE365_TENANT_ID
# The Office 365 resource your app wants to access.
# As you want to access Office 365 Calendar, Contact, and Mail APIs this is
# "https://outlook.office365.com/".
try:
    OFFICE365_RESOURCE = settings.OFFICE365_RESOURCE
except AttributeError:
    OFFICE365_RESOURCE = "https://outlook.office365.com/"
