# -*- encoding: utf-8 -*-
from setuptools import setup, find_packages

version = '0.1'

if __name__ == '__main__':
    setup(
        name='office365',
        version=version,
        description="Office365 Client Library",
        classifiers=["Development Status :: 4 - Beta",
                     "Intended Audience :: Developers",
                     "License :: OSI Approved :: GNU General "
                     "Public License (GPL)",
                     "License :: OSI Approved :: Apache Software License",
                     "Operating System :: OS Independent",
                     "Programming Language :: Python",
                     "Topic :: Office/Business :: Scheduling",
                     "Topic :: Software Development :: Libraries "
                     ":: Python Modules"],
        keywords='',
        author='Venkatesh Bachu',
        author_email='venkatesh.bachu2008@gmail.com',
        url='https://bitbucket.org/venkatesh22/office365.git',
        license='GPL',
        packages=find_packages(),
        include_package_data=True,
        zip_safe=False,
        install_requires=[],
    )
